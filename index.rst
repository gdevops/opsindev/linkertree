
.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/opsindev/linkertree/rss.xml>`_

.. _opsindev_linkertree:

=====================================================================
**Liens opsindev**
=====================================================================

- https://linkertree.frama.io/pvergain/


- https://gdevops.frama.io/opsindev/news/
- https://gdevops.frama.io/opsindev/tuto-git
- https://gdevops.frama.io/opsindev/sysops
- https://gdevops.frama.io/opsindev/tuto-project
- https://gdevops.frama.io/opsindev/tutorial
- https://gdevops.frama.io/opsindev/ansible
- https://gdevops.frama.io/opsindev/dagger
- https://gdevops.frama.io/opsindev/docker
- https://gdevops.frama.io/opsindev/kubernetes
- https://gdevops.frama.io/opsindev/secops



